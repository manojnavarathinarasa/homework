import java.util.Scanner;
 
public class seven
{
    public static void main(String[] args) 
    {
        Scanner seven = new Scanner(System.in);
         
        System.out.print("How many pettern you want : ");
         
        int rows = seven.nextInt();
         
        System.out.println("(*Hi*)-(**It's for you**)-(*I'm Manoj*)");
         
        for (int i = 1; i <= rows; i++) 
        {
            int num;
             
            if(i%2 == 0)
            {
                num = 0;
                 
                for (int j = 1; j <= rows; j++)
                {
                    System.out.print(num);
                     
                    num = (num == 0)? 1 : 0;
                }
            }
            else
            {
                num = 1;
                 
                for (int j = 1; j <= rows; j++)
                {
                    System.out.print(num);
                     
                    num = (num == 0)? 1 : 0;
                }
            }
             
            System.out.println();
        }
         
        seven.close();
    }
}